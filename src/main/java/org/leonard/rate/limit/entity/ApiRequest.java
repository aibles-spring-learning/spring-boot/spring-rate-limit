package org.leonard.rate.limit.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(staticName = "of")
@Table(name = "api_request")
@Entity
@Data
@NoArgsConstructor
public class ApiRequest {
  /*
  This is the api path
   */
  @Id
  private String id;

  @Column(name = "request_count")
  private Integer requestCount;

  @Column(name = "begin_time")
  private Long beginTime;

  @Column(name = "duration")
  private Long duration;

  @Column(name = "max_request")
  private Integer maxRequest;

  public void increaseRequestByOne() {
    this.requestCount = this.requestCount + 1;
  }
}
