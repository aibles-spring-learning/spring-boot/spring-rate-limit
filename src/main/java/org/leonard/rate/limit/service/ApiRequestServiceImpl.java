package org.leonard.rate.limit.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.leonard.rate.limit.entity.ApiRequest;
import org.leonard.rate.limit.repository.ApiRequestRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class ApiRequestServiceImpl implements ApiRequestService {

  private final ApiRequestRepository repository;

  @Override
  public void createOrUpdate(ApiRequest apiRequest) {
    log.info("(createOrUpdate)apiRequest: {}", apiRequest);
    repository.save(apiRequest);
  }

  @Override
  public ApiRequest get(String id) {
    return repository.findById(id).orElse(null);
  }
}
