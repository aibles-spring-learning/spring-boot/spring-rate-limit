package org.leonard.rate.limit.service;

import org.leonard.rate.limit.entity.ApiRequest;

public interface ApiRequestService {
  void createOrUpdate(ApiRequest apiRequest);

  ApiRequest get(String id);
}
