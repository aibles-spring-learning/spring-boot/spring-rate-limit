package org.leonard.rate.limit.repository;

import org.leonard.rate.limit.entity.ApiRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiRequestRepository extends JpaRepository<ApiRequest, String> {
}
