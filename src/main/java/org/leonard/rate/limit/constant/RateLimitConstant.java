package org.leonard.rate.limit.constant;

public class RateLimitConstant {
  private RateLimitConstant() {

  }

  public static final Integer MAX_REQUEST_PER_MINUTE = 3;
  public static final Long MILLI_SECONDS_OF_MINUTE = 60_000L;
}
