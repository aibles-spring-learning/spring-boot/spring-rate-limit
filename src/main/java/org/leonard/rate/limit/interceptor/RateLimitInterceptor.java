package org.leonard.rate.limit.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.leonard.rate.limit.entity.ApiRequest;
import org.leonard.rate.limit.service.ApiRequestService;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.servlet.HandlerInterceptor;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Objects;

import static org.leonard.rate.limit.constant.RateLimitConstant.MAX_REQUEST_PER_MINUTE;
import static org.leonard.rate.limit.constant.RateLimitConstant.MILLI_SECONDS_OF_MINUTE;

@Component
@RequiredArgsConstructor
@Slf4j
public class RateLimitInterceptor implements HandlerInterceptor {

  private final ApiRequestService apiRequestService;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    log.info("(preHandle)request: {}, response: {}, handler: {}", request, response, handler);

    var apiRequestId = request.getMethod() + ":" + request.getServletPath();

    var currentTimeLong = LocalDateTime.now().getLong(ChronoField.MILLI_OF_SECOND);

    var apiRequest = apiRequestService.get(apiRequestId);
    if (Objects.isNull(apiRequest)) {
      apiRequestService.createOrUpdate(
          ApiRequest.of(
              apiRequestId,
              1,
              currentTimeLong,
              MILLI_SECONDS_OF_MINUTE,
              MAX_REQUEST_PER_MINUTE
          )
      );
      return true;
    }

    // In case apiRequest not null, check if request time is out of duration from begin time, re-index count request
    Assert.notNull(apiRequest.getBeginTime(), "Api request beginTime can not be null");
    if (currentTimeLong - apiRequest.getBeginTime() > MILLI_SECONDS_OF_MINUTE) {
      apiRequest.setBeginTime(currentTimeLong);
      apiRequest.setMaxRequest(1);
      return true;
    }

    // In case time is valid for count request, check request is greater than max request?
    if (apiRequest.getRequestCount() >= apiRequest.getMaxRequest()) {
      response.sendError(429, "To many request");
      return false;
    }

    // In other case, increase request count by 1
    apiRequest.increaseRequestByOne();
    apiRequestService.createOrUpdate(apiRequest);
    return true;
  }
}
